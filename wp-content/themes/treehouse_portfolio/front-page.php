<?php get_header(); ?>

<!-- front-page.php is the template for static front-page -->
    <section class="row">
        <div class="small-12 columns text-center">
            <div class="leader">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <h1><?php the_title(); ?></h1>
                    <p><?php the_content(); ?></p>        
                <?php endwhile; else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php endif; ?>
            </div>
        </div>
    </section>

<!-- Paste loop code to list out portfolio  -->
<?php get_template_part('content', 'portfolio'); ?>

<?php get_footer(); ?>