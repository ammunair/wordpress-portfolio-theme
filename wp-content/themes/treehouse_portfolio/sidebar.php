<!--The sidebar code for creating widgets -->
<div class="small-12 medium-4 medium-pull-8 columns">
    <div class="secondary">
        <?php if(!dynamic_sidebar('blog')): ?> <!-- 'blog' was passed as parameter to wpt_createwidget fn -->
            <h2 class ="module-heading">Sidebar Setup</h2>
            <p>Please add widget from admin section</p>
        <?php endif; ?>
    </div>
</div>