<?php get_header(); ?>

<!-- The default template for controlling static pages -->

    <section class="row">
        <div class="small-12 columns text-center">
            <div class="leader">
                M at the page template
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <h1><?php the_title(); ?></h1>
                    <p><?php the_content(); ?></p>        
                <?php endwhile; else : ?>
                    <p><?php _e( 'Sorry, no pages matched your criteria.' ); ?></p>
                <?php endif; ?>

            </div>
        </div>
    </section>

<?php get_footer(); ?>
