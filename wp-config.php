<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_theme_portfolio');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '}`.94I=ANww|BU^x?1?D%aQJYN9O{JOd`l jn;<GyiC)!XH|htda`N|~=_eK$j;g');
define('SECURE_AUTH_KEY',  '~L&$7[YmtD<sU#)X8X[3=*.2j!1K,%<HjPG/0FR=kdEo>:;SQEzz-hpv`Fn.3of6');
define('LOGGED_IN_KEY',    '{p4PZ0S*3{n/qY+!uLAMlMKK18_97n,>=pOJK6r&u6/Og_-i`z4Q`j&cvi[+Fb>o');
define('NONCE_KEY',        'o*a2wRFt06=][E3yD{$X*;~UY0&p[lW=kU<(q!?,,CP2V13l]`3| :)hPm7kdv6m');
define('AUTH_SALT',        'D*PTOSSbzjpE]_3_+rp6[ZN/{vNT+ 2Knu B<F0>VB2*-,&NC:|s`t&z(my@A1&I');
define('SECURE_AUTH_SALT', 'rD>1jPeO6w~;t@VxOZYx! w>,Ozgnp:ko(oUvN(l]fZ6YJ|fpK[4Z@iGNVJ<w~rL');
define('LOGGED_IN_SALT',   'z@6kI.JO4Yn9eFB,qi H4$sU,L%q2y5SX)&bhzz[Y#@|>%+BF:]McpDs |$`i,(c');
define('NONCE_SALT',       '@Jm?`|T8PER[b})GwsWYiJFFn*3h{;z[`)JZe}]>S=elq+%?}ljxTgn6f`91|w7T');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_portfolio';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
